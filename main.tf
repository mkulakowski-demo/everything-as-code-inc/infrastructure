terraform {
  backend "http" {
  }
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "=3.7.0"
    }
  }
}

provider "google" {
  project = "group-cs-9b54eb"
  region  = "us-central1"
  zone    = "us-central1-a"
}

variable "gitlab_token" {
  type  = string
}

provider "gitlab" {
    token = var.gitlab_token
}

resource "google_container_cluster" "production-cluster" {
  name                = "mkulakowski-everythingascode-production"
  initial_node_count  = "1"
}

resource "gitlab_group_cluster" "bar" {
  group                         = "13233350" //ID of Everything as code group, expose as variable
  name                          = "production"
  domain                        = "everythingascode.lab.kulakowski.io" // is this a wildcard? expose as variable, provision a record within lab.kulakowski.io zone
  enabled                       = true
  kubernetes_api_url            = "${google_container_cluster.production-cluster.endpoint}" 
  kubernetes_token              = "PLACEHOLEDER_VALUE==" // requires creating kubernetes account to set this up
  kubernetes_ca_cert            = "${google_container_cluster.production-cluster.master_auth.0.cluster_ca_certificate}"
  kubernetes_authorization_type = "rbac"
  environment_scope             = "production"
  management_project_id         = "29316819" // ID of Everything as Code / Cluster Management
}
