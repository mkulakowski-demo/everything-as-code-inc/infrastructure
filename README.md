# Infrastructure

This project defines kubernetes clusters for testing and production as well as production database. This helps observe separation of duties. Both clusters get registered back to GitLab at the group level.
